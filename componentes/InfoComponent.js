import React, { Component } from 'react';
import { Text, ScrollView, View } from 'react-native';
import { Card } from 'react-native-elements';
import { SafeAreaView, FlatList } from 'react-native';
import { ListItem, Avatar } from 'react-native-elements';


import { EXCURSIONES } from '../comun/excursiones';
import { CABECERAS } from '../comun/cabeceras';
import { ACTIVIDADES } from '../comun/actividades';

function RenderInfo(props) {

    return (
        <Card>
            <Card.Title>Un poquito de historia</Card.Title>
            <Card.Divider />
            <Text style={{ margin: 20 }}>
                El nacimiento del club de montaña Gaztaroa se remonta a la
                primavera de 1976 cuando jóvenes aficionados a la montaña y
                pertenecientes a un club juvenil decidieron crear la sección
                montañera de dicho club. Fueron unos comienzos duros debido sobre
                todo a la situación política de entonces. Gracias al esfuerzo
                económico de sus socios y socias se logró alquilar una bajera.
                Gaztaroa ya tenía su sede social.
                Desde aquí queremos hacer llegar nuestro agradecimiento a todos
                los montañeros y montañeras que alguna vez habéis pasado por el
                club aportando vuestro granito de arena.
                Gracias!
            </Text>
        </Card>
    );
}

function RenderListaInfo(props) {
    console.log(props.actividades);
    const renderCalendarioItem = ({item, index}) => {
        return (
            <Card>
            <Card.Title>Un poquito de historia</Card.Title>
            <Card.Divider />
            <ListItem
                key={index}
                bottomDivider>
                <Avatar source={{uri: baseUrl + excursion.imagen}} />
                <ListItem.Content>
                    <ListItem.Title>{item.nombre}</ListItem.Title>
                    <ListItem.Subtitle>{item.descripcion}</ListItem.Subtitle>
                </ListItem.Content>
            </ListItem> 
            </Card>
            
        );
    };
    return (
        <SafeAreaView>
            <FlatList 
                data={props.actividades}
                renderItem={renderCalendarioItem}
                keyExtractor={item => item.id.toString()}
            />
        </SafeAreaView>
    );
}
class Info extends Component {
    constructor(props) {
        super(props);
        this.state = {
            excursiones: EXCURSIONES,
            cabeceras: CABECERAS,
            actividades: ACTIVIDADES
        };
    }

    render() {

        return (
            <View>
                <RenderInfo />
                <RenderListaInfo actividades={this.state.actividades}/>
            </View>
            
        );
    }
}

export default Info;