import React, { Component } from 'react';
import Calendario from './CalendarioComponent.js';
import { EXCURSIONES } from '../comun/excursiones';
import { View, StyleSheet, Image, Text } from 'react-native';
import DetalleExcursion from './DetalleExcursionComponent.js';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Constants from 'expo-constants';
import Home from './HomeComponent';
import Contacto from './ContactoComponent.js';
import Info from './InfoComponent.js';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { Icon } from 'react-native-elements';
import { SafeAreaView } from 'react-native-safe-area-context';
import { colorGaztaroaClaro } from '../comun/comun';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    drawerHeader: {
        backgroundColor: colorGaztaroaClaro,
        height: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
});

function CustomDrawerContent(props) {
    return (
        <DrawerContentScrollView {...props}>
            <SafeAreaView style={styles.container} forceInset={{
                top: 'always',
                horizontal: 'never'
            }}>
                <View style={styles.drawerHeader}>
                    <View style={{ flex: 1 }}>
                        <Image source={require('../public/imagenes/logo.png')} style={styles.drawerImage} />
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={styles.drawerHeaderText}> Gaztaroa</Text>
                    </View>
                </View>
                <DrawerItemList {...props} />
            </SafeAreaView>
        </DrawerContentScrollView>
    );
}


function HomeNavegador() {
    return (
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
                headerMode: "screen",
                headerShown: true,
                headerTintColor: '#fff',
                headerStyle: { backgroundColor: colorGaztaroaClaro },
                headerTitleStyle: { color: '#fff' },
            }}
        >
            <Stack.Screen
                name="HomeNavegador"
                component={Home}
                options={{
                    title: 'Campo Base',
                }}
            />
        </Stack.Navigator>
    );
}
function ContactoNavegador() {
    return (
        <Stack.Navigator
            initialRouteName="Contacto"
            screenOptions={{
                headerMode: "screen",
                headerShown: true,
                headerTintColor: '#fff',
                headerStyle: { backgroundColor: colorGaztaroaClaro },
                headerTitleStyle: { color: '#fff' },
            }}
        >
            <Stack.Screen
                name="ContactoNavegador"
                component={Contacto}
                options={{
                    title: 'Contacto',
                }}
            />
        </Stack.Navigator>
    );
}
function InfoNavegador() {
    return (
        <Stack.Navigator
            initialRouteName="Info"
            screenOptions={{
                headerMode: "screen",
                headerShown: true,
                headerTintColor: '#fff',
                headerStyle: { backgroundColor: colorGaztaroaClaro },
                headerTitleStyle: { color: '#fff' },
            }}
        >
            <Stack.Screen
                name="InfoNavegador"
                component={Info}
                options={{
                    title: 'Info',
                }}
            />
        </Stack.Navigator>
    );
}
function DrawerNavegador() {
    return (
        <Drawer.Navigator
            drawerStyle={{
                backgroundColor: colorGaztaroaClaro,
            }}
            initialRouteName="Home"
            drawerContent={props => <CustomDrawerContent {...props} />}
        >
            <Drawer.Screen name="Home" component={HomeNavegador} 
            options={{
                drawerIcon: ({ tintColor }) => (
                    <Icon
                        name='home'
                        type='font-awesome'
                        size={22}
                        color={tintColor}
                    />
                )
            }}/>
            <Drawer.Screen name="Calendario" component={CalendarioNavegador} 
            options={{
                drawerIcon: ({ tintColor }) => (
                    <Icon
                        name='calendar'
                        type='font-awesome'
                        size={22}
                        color={tintColor}
                    />
                )
            }}/>
            <Drawer.Screen name="Contacto" component={ContactoNavegador}
                options={{
                    drawerIcon: ({ tintColor }) => (
                        <Icon
                            name='address-card'
                            type='font-awesome'
                            size={22}
                            color={tintColor}
                        />
                    )
                }}
            />
            <Drawer.Screen name="Info" component={InfoNavegador} 
            options={{
                drawerIcon: ({ tintColor }) => (
                    <Icon
                        name='info-circle'
                        type='font-awesome'
                        size={22}
                        color={tintColor}
                    />
                )
            }}/>
        </Drawer.Navigator>
    );
}

function CalendarioNavegador() {
    return (
        <Stack.Navigator
            initialRouteName="Calendario"
            screenOptions={{
                headerMode: "screen",
                headerShown: true,
                headerTintColor: '#fff',
                headerStyle: { backgroundColor: colorGaztaroaClaro },
                headerTitleStyle: { color: '#fff' },
            }}
        >
            <Stack.Screen
                name="Calendario"
                component={Calendario}
                options={{
                    title: 'Calendario Gaztaroa',
                }}
            />
            <Stack.Screen
                name="DetalleExcursion"
                component={DetalleExcursion}
                options={{
                    title: 'Detalle Excursión',
                }}
            />
        </Stack.Navigator>
    );
}
class Campobase extends Component {
    constructor(props) {
        super(props);
        this.state = {
            excursiones: EXCURSIONES,
            seleccionExcursion: null
        };
    }
    onSeleccionExcursion = (excursionId) => {
        this.setState({ seleccionExcursion: excursionId })
    }
    render() {
        return (
            <NavigationContainer>
                <View style={{ flex: 1, paddingTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight }}>
                    <DrawerNavegador />
                </View>
            </NavigationContainer>
        );
    }
}
export default Campobase;