import { StatusBar } from 'expo-status-bar';
import React from 'react';
import Campobase from './componentes/CampoBaseComponent';

import { StyleSheet, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Campobase/>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
});